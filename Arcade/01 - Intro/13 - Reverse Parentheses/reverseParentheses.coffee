reverseString = (str) ->
  str.split('').reverse().join ''

reverseParentheses = (s) ->
  while s.includes('(')
    l = s.lastIndexOf('(')
    r = s.indexOf(')', s.lastIndexOf('('))
    s = s.slice(0, l) + reverseString(s.slice(l + 1, r)) + (if r + 1 == s.length then s.slice(r, -1) else s.slice(r + 1))
  s
