function areEquallyStrong(yourLeft, yourRight, friendsLeft, friendsRight)
    yourPower = yourLeft + yourRight
    yourStronger = math.max(yourLeft, yourRight)
    friendsPower = friendsLeft + friendsRight
    friendsStronger = math.max(friendsLeft, friendsRight)
    
    return (yourPower == friendsPower) and (yourStronger == friendsStronger)
end
