function electionsWinners(votes, k) {
    var maxVotes = votes.reduce(function(a, b){
            return Math.max(a, b);
        });
        
    var candidatesCanWin = votes.reduce(function(n, val){
        return n + (val + k > maxVotes);
    }, 0);
    
    //If the k and candidatesCanWin are equals zero, we must check the count of values equals maxVotes
    if(k == 0 && candidatesCanWin == 0){
        var equalsMaxVotes = votes.reduce(function(n, val){
            return n + (val == maxVotes);
        }, 0);
        
        //If equalsMaxVotes > 1, then there are candidates who have same number of votes, so we must return zero
        candidatesCanWin = equalsMaxVotes > 1 ? 0 : equalsMaxVotes;
    }
    
    return candidatesCanWin;
}