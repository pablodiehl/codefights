function minesweeper(matrix) {
    let rows = matrix.length,
        cols = matrix[0].length,
        i, j, mines = [];
        
    for(i=0; i<rows; i++){
        mines[i] = Array(cols).fill(0);
    }

    for(i=0; i<rows; i++){
        for(j=0; j<cols; j++){
            mines[i][j]=0;
            if(i-1 >= 0 && j-1 >= 0 && matrix[i-1][j-1]){
                mines[i][j]++;
            }
            if(j-1 >= 0 && matrix[i][j-1]){
                mines[i][j]++;
            }
            if(i+1 < rows && j > 0 && matrix[i+1][j-1]){
                mines[i][j]++;
            }
            if(i-1 >= 0 && matrix[i-1][j]){
                mines[i][j]++;
            }
            if(i+1 < rows && matrix[i+1][j]){
                mines[i][j]++;
            }
            if(i-1 >= 0 && j+1 < cols && matrix[i-1][j+1]){
                mines[i][j]++;
            }
            if(j+1 < cols && matrix[i][j+1]){
                mines[i][j]++;
            }
            if(i+1 < rows && j+1 < cols && matrix[i+1][j+1]){
                mines[i][j]++;
            }
        }
    }

    return mines;
}
