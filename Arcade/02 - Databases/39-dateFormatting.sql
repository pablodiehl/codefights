CREATE PROCEDURE dateFormatting()
BEGIN
	SELECT STR_TO_DATE(date_str, '%Y-%m-%d') AS date_iso
    FROM documents;
END