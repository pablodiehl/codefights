function fileNaming(names) {
    var i, j,
        name,
        result = [],
        size = names.length;
    for (i = 0; i < size; i++) {
        j = 1;
        if (result.includes(names[i])) {
            name = names[i] + '(' + j + ')';
            while (result.includes(name)) {
                j++;
                name = names[i] + '(' + j + ')';
            }
            result.push(name);
        } else {
            result.push(names[i]);
        }
    }
    return result;
}
