function differentSquares(matrix) {
    var i, j, aux,
        arr = [],
        result = 0,
        rowSize = matrix.length,
        colSize = matrix[0].length;
        
        
    for(i=1; i < rowSize; i++){
        for(j=1; j < colSize; j++){
            aux = matrix[i][j] + ','+ matrix[i-1][j] + ','+ matrix[i][j-1] + ','+ matrix[i-1][j-1];
            if(arr.indexOf(aux) === -1){
                arr.push(aux);
                result++;
            } 
        }
    }
        
    return result;
}