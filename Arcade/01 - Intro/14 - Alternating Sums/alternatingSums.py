def alternatingSums(a):
    team1 = a[::2] #Get only even indexes of the list
    team2 = a[1::2] #Get only odd indexes of the list

    #Return the sum of each list
    return [sum(team1), sum(team2)];