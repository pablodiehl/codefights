def addBorder(picture):
    borderLine = '*' * (len(picture[0])+2) #It create a String with multiple '*'s... #PythonRules! 
    
    #Let's initiate a new list
    borderedPicture = []
    borderedPicture.append(borderLine)
    
    #Let's copy the picture inserting the lateral borders
    for i in range(len(picture)):
        borderedPicture.append('*' + picture[i] + '*')
    
    #Finally, let's insert the  bottom border before returning the bordered picture
    borderedPicture.append(borderLine)
    
    return borderedPicture;