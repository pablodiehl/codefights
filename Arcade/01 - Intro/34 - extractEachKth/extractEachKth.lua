function extractEachKth(inputArray, k)
  newArray = {}
  for key, value in pairs(inputArray) do
    if(key % k ~= 0) then
      table.insert(newArray, value)
    end
  end
  return newArray
end