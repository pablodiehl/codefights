CREATE PROCEDURE soccerGameSeries()
BEGIN
	SELECT 
    CASE
        WHEN(
            SUM(
                CASE 
                  WHEN first_team_score > second_team_score THEN 1
                  WHEN second_team_score > first_team_score THEN -1
                  ELSE 0
                END
            )
        ) > 0 THEN 1
        WHEN (
            SUM(
                CASE 
                  WHEN first_team_score > second_team_score THEN 1
                  WHEN second_team_score > first_team_score THEN -1
                  ELSE 0
                END
            )
        ) < 0 THEN 2
        ELSE(
            CASE
                WHEN SUM(first_team_score) > SUM(second_team_score) THEN 1
                WHEN SUM(first_team_score) < SUM(second_team_score) THEN 2
            ELSE(
                CASE
                    WHEN (SUM(CASE WHEN match_host = 2 THEN first_team_score ELSE 0 END) > SUM(CASE WHEN match_host = 1 THEN second_team_score ELSE 0 END)) THEN 1
                    WHEN (SUM(CASE WHEN match_host = 2 THEN first_team_score ELSE 0 END) < SUM(CASE WHEN match_host = 1 THEN second_team_score ELSE 0 END)) THEN 2
                    ELSE 0
                END
            )
            END
        )
    END AS winner
    FROM scores;
END