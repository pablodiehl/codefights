function palindromeRearranging(inputString) {
    var size  = inputString.length,
        oddOccurrences = 0, chars = {},
        property, i;
    
    //Let's break the string counting the occurence of each character 
    for (i = 0; i < size; i++) {
       chars[inputString[i]] = (chars[inputString[i]] || 0) + 1;
    }
    
    //Now let's check how many odds and evens number of char occurences we have
    for (property in chars) {
        if(chars[property] % 2 > 0){
            oddOccurrences++;
        }
    }
    
    if(oddOccurrences > 1){
        return false;
    }
    
    return true;
}
