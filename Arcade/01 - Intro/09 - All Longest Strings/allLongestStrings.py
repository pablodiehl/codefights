def allLongestStrings(inputArray):
    size = len(inputArray)
    longest = 0
    words = []
    
    for i in range(0, size):
        if len(inputArray[i]) > longest:
            longest = len(inputArray[i])
            words = [inputArray[i]]
        elif(len(inputArray[i]) == longest):
            words.append(inputArray[i])
    
    
    return words