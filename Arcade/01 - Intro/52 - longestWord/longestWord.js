function longestWord(text) {
    var stringArray = text.match(/(\w|\s)*\w(?=")|\w+/g);
    
    return stringArray.reduce(function (a, b) { return a.length > b.length ? a : b; })
}