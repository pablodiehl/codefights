function isBeautifulString(inputString) {
    let map = mapChars(inputString),
        chars = Object.keys(map).sort(),
        i;
    for (i = 1; i < chars.length; i++) {
        if (map[chars[i]] > map[chars[i - 1]]) return false;
    }
    return true;
}

function mapChars(s) {
    let map = {};
    Array
        .from(Array(26).keys())
        .map(i => String.fromCharCode(i + 97))
        .forEach(c => map[c] = 0);
    s.split('').forEach(c => map[c]++)
    return map;
}