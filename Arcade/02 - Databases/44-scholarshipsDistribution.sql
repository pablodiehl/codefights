CREATE PROCEDURE scholarshipsDistribution()
BEGIN
	SELECT candidate_id as student_id
    FROM candidates
    WHERE candidate_id NOT IN(
        SELECT student_id AS candidate_id
        FROM detentions
    );
END