function longestDigitsPrefix(inputString) {
    let i=0,
        size = inputString.length,
        response = '';

    while(i<size){
        if(inputString[i].match(/\d+/g) == null){
            break;
        } else {
            response += inputString[i];
            i++;
        }
    }
    
    return response;
}