adjacentElementsProduct = (inputArray) ->
    max = inputArray[0] * inputArray[1]
    limit = inputArray.length - 1
    
    for i in [0...limit]
        line = inputArray[ i ] * inputArray[ i + 1 ]
        # This is like a ternary operator in CoffeeScript
        max = if line > max then line else max 
    max
        
