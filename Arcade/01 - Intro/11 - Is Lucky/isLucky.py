def isLucky(n):
    n = [int(x) for x in str(n)] #Break the number into a array

    half = int(len(n)/2) #Get the half point of the array
    
    #Let's split the array into two halves
    p1 = n[:half]
    p2 = n[half:]
    
    #Verify the sum of each half and return if they are equals
    return sum(p1) == sum(p2)