function growingPlant(upSpeed, downSpeed, desiredHeight) {
    let currentHeight = 0,
        days = 0;
    while(currentHeight < desiredHeight){
        days++;
        currentHeight += upSpeed;
        if(currentHeight >= desiredHeight){
            break;
        }
        currentHeight -= downSpeed;
    }
    
    return days;
}
