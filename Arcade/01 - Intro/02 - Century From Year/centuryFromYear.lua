function centuryFromYear(year)
  if year % 100 > 0 then
    return math.floor(year/100) + 1
  end
  
  return math.floor(year/100)
end
