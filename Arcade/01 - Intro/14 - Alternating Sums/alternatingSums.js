function alternatingSums(a) {
    let size = a.length,
        team1 = [],
        team2 = [],
        i;
    
    //Split the array into the two teams
    for(i=0; i<size; i++){
        if(i % 2 == 0){
            team1.push(a[i]);
        } else {
            team2.push(a[i]);
        }
    }
    
    team1 = team1.reduce((sum, value) => sum + value, 0); //Get team1 sum
    team2 = team2.reduce((sum, value) => sum + value, 0); //Get team2 sum
    
    return [team1, team2];
}