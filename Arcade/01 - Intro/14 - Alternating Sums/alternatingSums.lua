function reduce(table)
    local sum = 0
    
    for k,v in pairs(table) do
        sum = sum + v
    end
    
    return sum
end

function alternatingSums(a)
    size = #a
    team1 = {}
    team2 = {}
    
    -- Split the array into the two teams
    for i=1, size do
        if (i % 2 == 1) then
            table.insert(team1, a[i])
        else
            table.insert(team2, a[i])
        end
    end
    
    
    --Get the team sums
    team1 = reduce(team1)
    team2 = reduce(team2)
    
    
    
    return {team1, team2}
end