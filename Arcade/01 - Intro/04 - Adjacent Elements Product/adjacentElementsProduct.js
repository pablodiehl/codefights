function adjacentElementsProduct(inputArray) {
    let i,
        max = inputArray[0] * inputArray[1],
        len = inputArray.length;
    
    for(i=0; i<len; i++){
        if(inputArray[i] * inputArray[i+1] > max){
            max = inputArray[i] * inputArray[i+1];
        }
    }
    
    return max;
}
