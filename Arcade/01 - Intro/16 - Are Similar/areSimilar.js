function areSimilar(a, b) {
    let valueDifference = a.filter(value => b.indexOf(value) == -1);
    
    //If the arrays have some different value, they won't fit the requirements 
    if(valueDifference > 0){
        return false;
    } else {
        let indexDifference = a.filter((element, index) => element != b[index]);
        console.log(valueDifference.length, indexDifference.length);
        //If the arrays has more than 2 different values searchin by index, than they won't fit the requirements 
        if(indexDifference.length > 2){
            return false;
        } else {
            //If everything didn't work, let's sort the arrays and compare them, if they are equals, then they really fit this exercise description
            a.sort();
            b.sort();
            
            
            return JSON.stringify(a) == JSON.stringify(b); //JSON.stringify is the most silly - but simple - way to verify if two arrays have equals elements in Javascript 
        }
    }
}
