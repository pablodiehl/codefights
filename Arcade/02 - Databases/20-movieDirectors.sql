CREATE PROCEDURE movieDirectors()
BEGIN
	SELECT director
    FROM (
        SELECT director, SUM(oscars) as oscars
        FROM moviesInfo
        WHERE year > 2000
        GROUP BY director
    ) directors
    WHERE directors.oscars > 2
    ORDER BY director ASC;
END