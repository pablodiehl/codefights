def sortByHeight(a):
    #Copy and order the array
    ordered = list(a)
    ordered.sort(reverse=True) #Let's make it descending so we can pop the values correctly below
    
    
    #Remove all negative values from the ordered array
    ordered = [item for item in ordered if item >= 0]
      
    #Reorganize the array "a" with the ordered values when needed
    for i in range(len(a)):
        if(a[i] >= 0):
            a[i] = ordered.pop()
    
    return a;