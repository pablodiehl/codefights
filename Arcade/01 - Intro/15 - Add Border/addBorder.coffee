addBorder = (picture) ->
    result = []
    result[0] = Array(picture[0].length + 3).join '*'
    
    for value in picture
        result.push('*' + value + '*')
    
    result[result.length] = result[0]
    
    result