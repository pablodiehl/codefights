function areEquallyStrong(yourLeft, yourRight, friendsLeft, friendsRight) {
    let yourPower = yourLeft + yourRight,
        yourStronger = Math.max(yourLeft, yourRight),
        friendsPower = friendsLeft + friendsRight,
        friendsStronger = Math.max(friendsLeft, friendsRight);
    
    return (yourPower == friendsPower) && (yourStronger == friendsStronger);
}
