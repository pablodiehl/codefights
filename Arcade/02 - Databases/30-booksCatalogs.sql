CREATE PROCEDURE booksCatalogs()
BEGIN
	SELECT  substring_index(ExtractValue(xml_doc, '//catalog//book//author'),' ', 2) AS author
    FROM catalogs
    ORDER BY author;
END