function boxBlur(image) {
    var i, j, di, dj, 
        sum, line,
        rows = image.length - 1,
        cols = image[0].length - 1,
        result = [];
        
    for(i=1; i < rows; i++){
        line = [];
        for(j=1; j < cols; j++){ 
            sum = 0;
            for(di = -1; di <= 1; di++){
                for(dj = -1; dj <= 1; dj++){
                    sum += image[i + di][j + dj];
                }
            }
            line.push(Math.floor(sum / 9));
        }
        result.push(line);
    }
    return result;
}