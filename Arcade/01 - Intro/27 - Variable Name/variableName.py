def variableName(name):
    #import re
    if not re.match(r'^\w+$', name) or name[0].isdigit():
        return False
    
    return True