function shapeArea(n) {
    let i, result = 0;
    n--;
    for(i=n; i>0; i--){
        result += i*4;
    }
    result++;
    return result;
}