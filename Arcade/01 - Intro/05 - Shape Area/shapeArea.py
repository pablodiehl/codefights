def shapeArea(n):
  result = 0
  for i in range(1,n):
    result += i*4
  
  return result + 1
