function sortByHeight(a) {
    let i,
        size = a.length,
        //Copy and order the array
        ordered = a.slice().sort(function(a, b) {
          return a - b;
        });
  
    
    //Remove all negative values from the ordered array
    ordered = ordered.filter(function(a){
        return a > 0;
    });
      
    //Reorganize the array "a" with the ordered values when needed
    for(i=0; i < size; i++){
        if(a[i] > 0){
            a[i] = ordered.shift(); //Shift is like a pop, but remove the first item of an array instead
        }
    }
    
    return a;
}