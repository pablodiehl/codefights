function makeArrayConsecutive2(statues) {
    let min = statues.reduce(function(a, b) {
        return Math.min(a, b);
    });
    let max = statues.reduce(function(a, b) {
        return Math.max(a, b);
    });
    return max - min - (statues.length - 1);
}
