function bishopAndPawn(bishop, pawn) {
    if((bishop[0] == pawn[0]) || (bishop[1] == pawn[1])){
        return false;
    }
    
    bishop = (bishop.charCodeAt(0) + parseInt(bishop[1])) % 2;
    pawn = (pawn.charCodeAt(0) + parseInt(pawn[1])) % 2;
    
    if(bishop !== pawn){
        return false;
    }
    
    return true;
}