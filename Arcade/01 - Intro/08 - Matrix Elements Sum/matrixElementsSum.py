def matrixElementsSum(matrix):
    rows = len(matrix)
    cols = len(matrix[0])
    total = 0
    
    for i in range(0, cols):
        for j in range(0, rows):
            if(matrix[j][i] == 0):
                break;
            else:
                total += matrix[j][i]
    
    return total;