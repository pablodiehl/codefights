//Check if the strings are different by just one char
function isValid(a, b){
    if(a === b){
        return false;
    }

    let i, size = a.length, diff = 0;

    for(i=0; i<size; i++){
      if(a[i] !== b[i]){
        diff++;
      }
    }

    if(diff !== 1){
      return false;
    }

    return true;
}

//Permutes an Array | Source: https://gist.github.com/jweinst1/f870d464daa7fba0c1d0
function permutator(inputArr) {
  var results = [];

  function permute(arr, memo) {
    var cur, memo = memo || [];

    for (var i = 0; i < arr.length; i++) {
      cur = arr.splice(i, 1);
      if (arr.length === 0) {
        results.push(memo.concat(cur));
      }
      permute(arr.slice(), memo.concat(cur));
      arr.splice(i, 0, cur[0]);
    }

    return results;
  }

  return permute(inputArr);
}


function stringsRearrangement(inputArray){
    let permutated = permutator(inputArray),
        sizeI = permutated.length,
        sizeJ = inputArray.length,
        i, j, valid;

    for(i=0; i<sizeI; i++){
        valid = 0;
        for(j=1; j<sizeJ; j++){
            if(!isValid(permutated[i][j-1], permutated[i][j])){
                continue;
            } else {
                valid++;
            }
        }
        if(valid === (sizeJ - 1)){
            return true;
        }
    }

    return false;
}
