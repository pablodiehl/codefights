function absoluteValuesSumMinimization(a) {
  let minimum = Infinity,
      size = a.length,
      element, i, j;

  for (i = 0; i < size; i++) {
    var sum = 0;
    for (j = 0; j < size; j++) {
      sum += Math.abs(a[i] - a[j]);
    }

    if (sum < minimum) {
      minimum = sum;
      element = a[i];
    }
  }

  return element;
}
