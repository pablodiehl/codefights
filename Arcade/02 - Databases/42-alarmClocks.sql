CREATE PROCEDURE alarmClocks()
BEGIN
    SET @date = (
        SELECT input_date
        FROM userInput LIMIT 1
    );
    SET @initialYear = YEAR(@date);
    SET @date = ADDDATE(@date, INTERVAL 7 DAY);
    
	WHILE YEAR(@date) = @initialYear DO
        INSERT INTO userInput VALUES (@date);
        SET @date = ADDDATE(@date, INTERVAL 7 DAY);
    END WHILE;

    SELECT input_date AS alarm_date
    FROM userInput;
END