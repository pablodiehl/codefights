matrixElementsSum = (matrix) ->
  rows = matrix.length
  cols = matrix[0].length
  total = 0
  i = 0
  
  while i < cols
    j = 0
    while j < rows
      if matrix[j][i] == 0
        break
      else
        total += matrix[j][i]
      j++
    i++
  total
