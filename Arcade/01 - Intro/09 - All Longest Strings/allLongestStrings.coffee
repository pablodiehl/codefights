allLongestStrings = (inputArray) ->
    longest = 0
    for text in inputArray
        text_size = text.length
        if text_size > longest
            longest = text_size
            result = [text]
        else if text_size == longest
            result.push(text)
    
    result