CREATE PROCEDURE userCountries()
BEGIN
    SELECT u.id, 
        CASE 
            WHEN c.country IS NULL 
            THEN 'unknown' 
            ELSE c.country 
        END as country
    FROM users u
    LEFT JOIN cities c
    ON u.city = c.city
    ORDER BY u.id asc;
END
