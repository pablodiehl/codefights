function differentSymbolsNaive(s) {
    // Turn the string into an array for a easier manipulation
    // Manipule the array with filter() to return a new array with only the unique characters
    // Return the length of the new array
    return s.split('').filter(function(item, i, ar){ return ar.indexOf(item) === i; }).join('').length;
}
