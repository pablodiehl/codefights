function messageFromBinaryCode(code) {
    //Break the string into parts of 8 characters each
    code = code.match(/.{1,8}/g)
    
    return code.map(function(e){
        //Converts binary string to ascii string
        return String.fromCharCode(parseInt(e, 2));
    }).join('');
}