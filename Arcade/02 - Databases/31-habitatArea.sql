CREATE PROCEDURE habitatArea()
BEGIN
    SET @g := (SELECT CONCAT("MULTIPOINT(", GROUP_CONCAT(x,' ',y, '') , ")") FROM places);
    SELECT ST_AREA(ST_ConvexHull(ST_GeomFromText(@g))) AS area;
END