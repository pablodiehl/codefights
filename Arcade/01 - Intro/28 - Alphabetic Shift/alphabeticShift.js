String.prototype.replaceAt = function(index, char) {
    return this.substr(0, index) + char + this.substr(index + char.length);
};

function alphabeticShift(inputString) {
    let size = inputString.length,
               charCodeAtIndex, i;
    
    inputString = inputString.split(''); //Convert the String into array so it will be easier to work with
    
    for(i=0; i<size; i++){
        if(inputString[i] == 'z'){
            inputString[i] = 'a';
        }else{
            inputString[i] = String.fromCharCode(inputString[i].charCodeAt(0) + 1);
        }
    }
    
    inputString = inputString.join(''); //Converts back to Javascript
    
    return inputString;
}