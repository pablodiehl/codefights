def minesweeper(matrix):
    result = [row[:] for row in matrix]
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            result[i][j] = 0
            # Top left
            if i-1 >= 0 and j-1 >= 0 and matrix[i-1][j-1]:
                result[i][j] += 1
            # Top
            if j-1 >= 0 and matrix[i][j-1]:
                result[i][j] += 1
            # Top right
            if i+1 < len(matrix) and j > 0 and matrix[i+1][j-1]:
                result[i][j] += 1
            # Left
            if i-1 >= 0 and matrix[i-1][j]:
                result[i][j] += 1
            # Right
            if i+1 < len(matrix) and matrix[i+1][j]:
                result[i][j] += 1
            # Bottom left
            if i-1 >= 0 and j+1 < len(matrix[i]) and matrix[i-1][j+1]:
                result[i][j] += 1
            # Bottom
            if j+1 < len(matrix[i]) and matrix[i][j+1]:
                result[i][j] += 1
            # Bottom right
            if i+1 < len(matrix) and j+1 < len(matrix[i]) and matrix[i+1][j+1]:
                result[i][j] += 1
    return result