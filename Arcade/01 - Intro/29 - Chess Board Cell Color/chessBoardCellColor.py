def chessBoardCellColor(cell1, cell2):
    cell1 = (ord(cell1[0]) + int(cell1[1])) % 2
    cell2 = (ord(cell2[0]) + int(cell2[1])) % 2

    return cell1 == cell2