function evenDigitsOnly(n) {
    n = n.toString();
    let size = n.length, i;
    for(i=0; i<size; i++){
        if(n[i] % 2 > 0){
            return false;
        }
    }
        
    return true;
}