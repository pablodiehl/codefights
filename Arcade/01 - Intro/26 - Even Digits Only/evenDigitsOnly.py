def evenDigitsOnly(n):
    n = str(n)
    for digit in map(int, n):
        if digit % 2 > 0:
            return False
    
    return True