function chessBoardCellColor(cell1, cell2)
      cell1 = (string.byte(cell1, 1) + tonumber(cell1:sub(2,2))) % 2
      cell2 = (string.byte(cell2, 1) + tonumber(cell2:sub(2,2))) % 2
      
      return cell1 == cell2
end
