function arrayMaxConsecutiveSum(inputArray, k) {
    let maxSum = 0,
        size = inputArray.length - k,
        sequenceLimit, aux, i, j;
    
    for(i=0; i<=size; i++){
        sequenceLimit = i + k;
        aux = 0;
        for(j=i; j<sequenceLimit; j++){
            aux += inputArray[j];
        }
        maxSum = aux > maxSum ? aux : maxSum;
    }
    
    return maxSum;
}
