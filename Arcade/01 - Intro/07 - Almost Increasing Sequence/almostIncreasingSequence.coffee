almostIncreasingSequence = (sequence) ->
  len = sequence.length
  i = 0
  while i < len
    if sequence[i] <= sequence[i - 1]
      if found
        return false
      found = true
      if i == 1 or i + 1 == len
        i++
        continue
      else if sequence[i] > sequence[i - 2]
        sequence[i - 1] = sequence[i - 2]
      else if sequence[i - 1] >= sequence[i + 1]
        return false
    i++
  true