function depositProfit(deposit, rate, threshold)
  years = 0
  while deposit < threshold do
    deposit = deposit + (deposit * (rate/100))
    years = years + 1
  end

  return years
end
