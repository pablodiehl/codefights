function lineEncoding(s) {
    var i,
        counter = 1, 
        result = '',
        size = s.length;

    for(i=0; i < size; i++){
        if(s[i] === s[i+1]){
            counter++;
        } else {
            result += counter + s[i];
            counter = 1;
        }
    }
    return result.replace(/1/g, '');
}