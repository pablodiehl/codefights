function addBorder(picture)
    borderLine = string.rep('*', (#picture[1]+2)) --It creates a String with multiple '*'s... #LuaRocks! 

    --Let's initiate a new table
    borderedPicture = {}
    
    --Let's copy the picture inserting the lateral borders
    for i=1, #picture do
        borderedPicture[i+1] =  '*' .. picture[i] .. '*'
    end
    
    --Finally, let's insert the top and bottom borders before returning the bordered picture
    borderedPicture[1] = borderLine
    borderedPicture[#borderedPicture+1] = borderLine
    
    return borderedPicture
end
