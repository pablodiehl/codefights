CREATE PROCEDURE correctIPs()
BEGIN
	SELECT * 
    FROM ips 
    WHERE IS_IPV4(ip)
    AND (
        ip REGEXP '^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{2,3}\.[0-9]{1,3}$' 
        OR ip REGEXP '^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{2,3}$'
    )
    ORDER BY id;
END