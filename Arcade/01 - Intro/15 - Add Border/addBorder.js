function addBorder(picture) {
    let borderedPicture = [], i,
        rows = picture.length,
        cols = picture[0].length,
        borderLine = new Array(cols + 3).join('*'); //Create a line with *s | Why that "+3"? 'cause array.join(char) insert the elements between the array arguments
    
    //Let's copy the picture inserting the lateral border
    for(i=0; i<rows; i++){
        borderedPicture[i+1] = '*' + picture[i] + '*';
    }
    
    //Now let's insert the top and bottom borders
    borderedPicture[0] = borderLine;
    borderedPicture[rows+1] = borderLine;
    
    return borderedPicture;
}
