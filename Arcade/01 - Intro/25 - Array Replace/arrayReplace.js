function arrayReplace(inputArray, elemToReplace, substitutionElem) {
    inputArray.forEach(function(value, index) {
        if(value == elemToReplace){
            inputArray[index] = substitutionElem;
        }
    });
    return inputArray;
}
