function variableName(name) {
    if(!(/^\w+$/i).test(name) || !isNaN(name[0])){
        return false;
    }
    
    return true;
}