function deleteDigit(n) {
    var i,
        size,
        array = [];
    
    n = n.toString();
    size = n.length;
    
    for(i=1; i <= size; i++){
      array.push(n.substring(0, i - 1) + n.substring(i, size));
    }
    
    return array.reduce(function(a, b) {
        return Math.max(a, b);
    });
}
