function arrayReplace(inputArray, elemToReplace, substitutionElem)
  for i=1, #inputArray do
    if inputArray[i] == elemToReplace then
      inputArray[i] = substitutionElem
    end
  end
  
  return inputArray
end
