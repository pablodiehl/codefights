def commonCharacterCount(s1, s2):
    quantity = 0
        
    for i in range(len(s1)):
        aux = s2.find(s1[i]);
        if(aux != -1):
            s2 = s2[:aux] + s2[aux+1:]
            quantity += 1
    
    return quantity
