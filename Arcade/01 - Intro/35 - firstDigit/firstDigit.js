function firstDigit(inputString) {
    // inputString.search(/\d+/g) will return the index of the first char that is a digit
    // So, we can return it using the command inputString[<index>]
    return inputString[inputString.search(/\d+/g)];
}