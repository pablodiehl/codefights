CREATE PROCEDURE orderingEmails()
BEGIN
	SELECT id, email_title, 
        IF(size >= 1048576, 
            CONCAT(FLOOR(size/1048576), " Mb"), 
            CONCAT(FLOOR(size/1024), " Kb")
        ) AS short_size 
    FROM emails 
    ORDER BY size DESC;
END