alternatingSums = (a) ->
    team1 = 0
    team2 = 0
    for value, index in a
        if(index % 2 == 0)
            team1 += value
        else
            team2 += value
            
    [team1, team2]