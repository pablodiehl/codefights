function allLongestStrings(inputArray) {
    let len = inputArray.length,
        longest = 0,
        words = [], i;
    
    for(i=0; i< len; i++){
        if(inputArray[i].length > longest){
            longest = inputArray[i].length;
            words = [inputArray[i]];
        } else if(inputArray[i].length == longest){
            words.push(inputArray[i]);
        }
    }
    
    return words;
}
