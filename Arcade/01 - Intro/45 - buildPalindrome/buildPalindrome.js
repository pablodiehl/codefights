function buildPalindrome(st){
    if(st == st.split('').reverse().join('')){
        return st;
    }
    
    var mirror = st.split('').reverse().join(''),
        palindrome = st + mirror.substr(1),
        size = st.length,
        checker, i;
    
    for(i = 2; i < size; i++){
        checker =  st + mirror.substr(i);
        if(checker == checker.split('').reverse().join('')){
            palindrome = checker;
        }
        
    }
    
    return palindrome;
}
