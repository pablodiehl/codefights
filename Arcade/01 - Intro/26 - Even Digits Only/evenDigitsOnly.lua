function evenDigitsOnly(n)
  n = tostring(n)
  for i=1, #n do
    if tonumber(n:sub(i,i)) % 2 > 0 then
      return false
    end
  end
  
  return true
end
