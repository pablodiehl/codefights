function commonCharacterCount(s1, s2) {
    let i, quantity = 0,
        aux, size = s1.length;
    s1 = s1.split('');
    s2 = s2.split('');
    
    
    for(i=0; i<size; i++){
        aux = s2.indexOf(s1[i]);
        if(aux !== -1){
            s2.splice(aux,1);
            quantity++;
        }
    }
    
    return quantity;
}
