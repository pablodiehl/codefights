function isIPv4Address(inputString) {
    inputString = inputString.split('.');
    let size = inputString.length, i;
    
    if(size !== 4){
        return false;
    }
    
    for(i=0; i<size; i++){
        if(isNaN(inputString[i]) || inputString[i] < 0 || inputString[i] > 255 || inputString[i] == ''){
            return false;
        }
    }
    
    return true;
}
