def arrayReplace(inputArray, elemToReplace, substitutionElem):
    for index, value in enumerate(inputArray):
        if(value == elemToReplace):
            inputArray[index] = substitutionElem
    
    return inputArray
