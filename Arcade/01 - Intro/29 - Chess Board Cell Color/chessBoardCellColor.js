function chessBoardCellColor(cell1, cell2) {
    cell1 = (cell1.charCodeAt(0) + cell1.charCodeAt(1)) % 2;
    cell2 = (cell2.charCodeAt(0) + cell2.charCodeAt(1)) % 2;
    return cell1 == cell2;
}