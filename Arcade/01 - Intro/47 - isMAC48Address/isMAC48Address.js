function isMAC48Address(inputString) {
    const ALLOWED = '0123456789ABCDEF';
    
    if(inputString.slice(-1) === '-'){
        return false;
    }
    
    inputString = inputString.split('-');
    var size = inputString.length, i;
    
    for(i=0; i<size; i++){
        if(inputString[i].length !== 2 ||
           ALLOWED.indexOf(inputString[i][0]) === -1 ||
           ALLOWED.indexOf(inputString[i][1]) === -1){
            return false;
        }
    }
    
    return true;
}