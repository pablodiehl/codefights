function digitsProduct(product) {
    if(product == 1){
        return 1;
    }
    
    if(product == 0){
        return 10;
    }
    
    var i,
        input = product,
        count = 0,
        result = '';
    
    for(i=9; i>1; i--){
        while(product % i == 0){
            count++;
            product /= i;
            result += i;
        }
    }
    
    if(result == ''){
        return -1;
    }
    
    var resultProd = result.split('').reduce(function(a,b){
                        return parseInt(a) * parseInt(b);
                    });

    if(input !== resultProd){
        if(input.toString().length > 1){
            return -1;
        }
    }
    
    return parseInt(result.split('').reverse().join(''));
}
