function chessKnight(cell) {
    const SIZE = 8,
          STEPS = [
            [-2, -1], [-1, -2], [1, -2], [2, -1],
            [2, 1], [1, 2], [-1, 2], [-2, 1]
        ];
    
    var i, col, row,
        result = 0;
    
    for(i=0; i < SIZE; i++){
        col = cell.charCodeAt(0) + STEPS[i][0];
        row = parseInt(cell[1]) + STEPS[i][1];
        if(col > 96 && col < 105 && row > 0 && row < 9){
            result++;
        }
    }
           
    return result;
}
