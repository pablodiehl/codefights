def areEquallyStrong(yourLeft, yourRight, friendsLeft, friendsRight):
    yourPower = yourLeft + yourRight
    yourStronger = max(yourLeft, yourRight)
    friendsPower = friendsLeft + friendsRight
    friendsStronger = max(friendsLeft, friendsRight)
    
    return (yourPower == friendsPower) and (yourStronger == friendsStronger)