function adjacentElementsProduct(inputArray)
    len = #inputArray - 1
    max = inputArray[1] * inputArray[2]

    for i=1, len do
        if inputArray[i] * inputArray[i+1] > max then
           max =  inputArray[i] * inputArray[i+1]
        end
    end

    return max
end