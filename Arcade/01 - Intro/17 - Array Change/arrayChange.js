function arrayChange(inputArray) {
    var smaller = inputArray[0],
        size = inputArray.length,
        counter = 0, i;
    
    for (i=1; i< size; i++) {
        if(smaller >= inputArray[i]){
            counter += (++smaller) - inputArray[i];
        } else {
            smaller = inputArray[i];
        }
    }
    return counter;
}
