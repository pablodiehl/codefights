CREATE PROCEDURE netIncome()
BEGIN
    SELECT year(date) AS year,
           quarter(date) AS quarter,
           SUM(profit) - SUM(loss) AS net_profit
    FROM accounting
    GROUP BY year(date), quarter(date)
    ORDER BY year(date), quarter(date);
END