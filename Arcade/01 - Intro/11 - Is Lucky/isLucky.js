function isLucky(n) {
    let half, size, part1;
    
    n = n.toString().split(''); //Turn the integer into array
    size = n.length;
    half = size/2;
    
    part1 = n.splice(0, half); 
    //Now part1 will be the first half of the array and n will 
    //be the second half
    //
    //For example if the array is ['1','2','3','4']
    //part1 will be ['1','2'] and n ['3','4']
    
    //Finally we reduce the array to find they total values
    part1 = part1.reduce(function(a, b) {
        return parseInt(a) + parseInt(b);
    });
    
    n = n.reduce(function(a, b) {
        return parseInt(a) + parseInt(b);
    });
    
    return part1 == n; //Return a boolean value
}