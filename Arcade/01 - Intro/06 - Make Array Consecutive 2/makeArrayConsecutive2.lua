function makeArrayConsecutive2(statues)
  min = math.min(unpack(statues))
  max = math.max(unpack(statues))
  return max - min - (#statues - 1)
end
