function sumUpNumbers(inputString) {
    var arrayNumbers = inputString.match(/[0-9]+/g);
    
    //Verify if there is numbers in the inputString
    if(arrayNumbers){
        return arrayNumbers.reduce(function (a, b) {
            return parseInt(a) + parseInt(b);
        }, 0);
    }
    
    return 0;
}