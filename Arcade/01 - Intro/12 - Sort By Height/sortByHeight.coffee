sortByHeight = (a) ->
  size = a.length
  ordered = a.slice().sort((a, b) ->
    a - b
  )

  #Remove all negative values from the ordered array
  ordered = ordered.filter((a) ->
    a > 0
  )

  #Reorganize the array "a" with the ordered values when needed
  i = 0
  while i < size
    if a[i] > 0
      a[i] = ordered.shift()
    i++
    
  a