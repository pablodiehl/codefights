function extractEachKth(inputArray, k) {
    return inputArray.filter(function(value, index){
        return (index + 1) % k !== 0;
    });
}
