CREATE PROCEDURE nullIntern()
BEGIN
	SELECT COUNT(*) AS number_of_nulls
    FROM departments
    WHERE TRIM(description) IN ('null', 'nil', '-')
    OR description IS NULL;
END