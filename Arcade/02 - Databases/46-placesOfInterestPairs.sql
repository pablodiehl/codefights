CREATE PROCEDURE placesOfInterestPairs()
BEGIN
    SELECT  s1.name as place1, s2.name as place2
    FROM sights s1 
    INNER JOIN sights s2
    WHERE SQRT(POWER(s1.x-s2.x, 2) + POWER(s1.y-s2.y, 2)) != 0
    AND SQRT( POWER(s1.x-s2.x, 2) + POWER(s1.y-s2.y, 2)) < 5 
    AND s1.name < s2.name
    ORDER BY s1.name, s2.name;
END