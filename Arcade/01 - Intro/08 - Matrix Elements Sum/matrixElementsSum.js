function matrixElementsSum(matrix) {
    let rows = matrix.length,
        cols = matrix[0].length,
        total = 0,
        i, j;
    
    for(i=0; i<cols; i++){
        for(j=0; j<rows; j++){
            if(matrix[j][i] == 0){
                break;
            } else {
                total += matrix[j][i];
            }
        }
    }
    
    return total;
}