function arrayMaximalAdjacentDifference(inputArray) {
    let size = inputArray.length, i,
        maximal = inputArray[1] - inputArray[0];
    for(i=1; i<size;i++){
        if(inputArray[i] - inputArray[i-1] > maximal){
            maximal = inputArray[i] - inputArray[i-1];
        }
        if(inputArray[i] - inputArray[i+1] > maximal){
            maximal = inputArray[i] - inputArray[i+1];
        }
    }
    
    return maximal;
}
