CREATE PROCEDURE orderOfSuccession()
BEGIN
	SELECT 
        CASE 
            WHEN (gender='M') THEN CONCAT('King ', name)
            ELSE CONCAT("Queen ", name)
        END AS name
    FROM Successors
    ORDER BY birthday ASC;
END