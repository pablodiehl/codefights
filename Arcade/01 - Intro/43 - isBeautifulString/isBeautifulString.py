def isBeautifulString(inputString):
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    times = []
    for letter in alphabet:
        times.append(inputString.count(letter))
    return times == sorted(times, reverse=True)