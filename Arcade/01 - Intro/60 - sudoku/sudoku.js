function hasDuplicates(str) {
    return new Set(str).size !== str.length;
}

function sudoku(grid) {
    const arrayColumn = (arr, n) => arr.map(x => x[n]);
  
    var i, j, str,
        gridSize = grid.length;
    
    for(i=0; i < gridSize; i++){
        str = grid[i].join('');
        console.log(str);
        if(hasDuplicates(str)){
            return false;
        }
  
        str = arrayColumn(grid, i).join('');
        if(hasDuplicates(str)){
            return false;
        }
    }

    for(i=0; i < gridSize; i=i+3){
        for(j=0; j < gridSize; j=j+3){
            str = '' +
                  grid[i][j] +
                  grid[i][j+1] +
                  grid[i][j+2] +
                  grid[i+1][j] +
                  grid[i+2][j] +
                  grid[i+1][j+1] +
                  grid[i+1][j+2] +
                  grid[i+2][j+1] +
                  grid[i+2][j+2];
            
            if(hasDuplicates(str)){
                return false;
            }
        }
    }
    
    
    return true;
}