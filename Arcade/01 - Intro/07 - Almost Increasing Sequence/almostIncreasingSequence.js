function almostIncreasingSequence(sequence) {
  let found = false,
      len = sequence.length,
      i;
  for (i=0; i<len ;i++) {
    if(sequence[i] <= sequence[i-1]) {
      if(found) {
        return false;
      }
      found = true;
      
      if(i === 1 || i + 1 === len) {
        continue;
      }
      else if (sequence[i] > sequence[i-2]) {
        sequence[i-1] = sequence[i-2];
      }
      else if(sequence[i-1] >= sequence[i+1]) {
        return false;
      }
    }
  }
  return true;
}