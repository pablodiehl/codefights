function digitDegree(n) {
    let times = 0, sum;
    while(n > 9){
        sum = 0;
        while (n) {
            sum += n % 10;
            n = Math.floor(n / 10);
        }
        
        n = sum;
        
        times++;
    }
    
    return times;
}
